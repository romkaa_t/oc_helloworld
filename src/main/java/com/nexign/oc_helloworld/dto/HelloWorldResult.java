package com.nexign.oc_helloworld.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
public class HelloWorldResult {

  @Getter
  String result = "Hello world";
}
