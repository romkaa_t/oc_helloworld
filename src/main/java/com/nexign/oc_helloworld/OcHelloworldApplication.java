package com.nexign.oc_helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OcHelloworldApplication {

  public static void main(String[] args) {
    SpringApplication.run(OcHelloworldApplication.class, args);
  }

}
