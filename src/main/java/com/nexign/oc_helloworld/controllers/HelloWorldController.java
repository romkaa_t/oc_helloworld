package com.nexign.oc_helloworld.controllers;

import com.nexign.oc_helloworld.dto.HelloWorldResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

  @GetMapping("/helloWorld")
  HelloWorldResult helloWorld() {
    return HelloWorldResult.builder().result("Hello world!").build();
  }

}